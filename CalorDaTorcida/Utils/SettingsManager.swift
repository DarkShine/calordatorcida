//
//  SettingsManager.swift
//  CalorDaTorcida
//
//  Created by Henrique Valcanaia on 9/10/15.
//  Copyright (c) 2015 DarkShine. All rights reserved.
//

import Foundation
import Parse

class SettingsManager: NSObject {
    
    static let sharedInstance = SettingsManager()
    var team:Team!
    
    private func loadTeamFrom(user:PFUser!, block: PFBooleanResultBlock){
        if let query = PFUser.query(){
            query.includeKey("team")
            query.getFirstObjectInBackgroundWithBlock({ (userFromParse: PFObject?, error: NSError?) -> Void in
                if error == nil{
                    self.team = userFromParse!.objectForKey("team") as! Team
                    block(true, nil)
                }else{
                    block(false, error)
                }
            })
        }
    }
    
    func loadTeam(block: PFBooleanResultBlock){
        SettingsManager.sharedInstance.loadTeamFrom(PFUser.currentUser()!, block: block)
    }
    
    func testLogin(){
        PFUser.logInWithUsername("test", password: "test")
    }
    
}
