//
//  RankingViewController.swift
//  CalorDaTorcida
//
//  Created by Pietro Degrazia on 8/27/15.
//  Copyright (c) 2015 DarkShine. All rights reserved.
//

import UIKit

class RankingViewController : UITableViewController{
    
    var teams = [Team]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadTeamsFromParse()
    }
    
    func loadTeamsFromParse(){
        let teamQuery = Team.query()
        teamQuery?.findObjectsInBackgroundWithBlock({ (result, error) -> Void in
            if error == nil{
                self.teams = result as! [Team]
                self.tableView.reloadData()
            } else{
                print(error)
            }
        })
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }

}

