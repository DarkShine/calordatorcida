//
//  LeaguesTableViewController.swift
//  CalorDaTorcida
//
//  Created by Pietro Degrazia on 9/10/15.
//  Copyright (c) 2015 DarkShine. All rights reserved.
//

import UIKit

class LeaguesTableViewController: UIViewController{
    
    
    @IBOutlet weak var tableView: UITableView!
    var leagues:[League] = [League]()
//    var countries = Set<String>()
    var leagueDic:NSDictionary?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        loadLeagues()
    }
    
    func loadLeagues(){
        let query = League.query()
        query?.findObjectsInBackgroundWithBlock({ (result, error) -> Void in
            print(result)
            if (error == nil){
                if let leaguesArray = result as? [League]{
                    self.leagues = leaguesArray
                    self.tableView.reloadData()
                    //                    for league in leagues{
                    //                        if((self.leagueDic?.objectForKey(league.country)) != nil){
                    //                            self.countries.insert(league.country)
                    //                            var leagueArray = self.leagueDic?.valueForKey(league.country) as! [League]
                    //                            leagueArray.append(league)
                    //                            self.leagueDic?.setValue(leagueArray, forKey: league.country)
                    //                        } else {
                    //
                    //                        }
                    //
                    //
                    //                        self.countries.insert(league.country)//CRIEI SET COM PAISES
                    //                    }
                }
            } else {
                print(error)
            }
        })
    }
}

extension LeaguesTableViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.performSegueWithIdentifier("gotoTeamsList", sender: indexPath.row)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "gotoTeamsList"){
            let vc = segue.destinationViewController as! TeamTableViewController
            vc.league = self.leagues[sender as! Int]
        }
    }
}
extension LeaguesTableViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.leagues.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let kLeagueCellReuseIdentifier = "LeagueCell"
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: kLeagueCellReuseIdentifier)
        let league = self.leagues[indexPath.row]
        cell.textLabel?.text = league.name
        
        return cell
    }
}