//
//  TeamTableViewController.swift
//  CalorDaTorcida
//
//  Created by Pietro Degrazia on 9/10/15.
//  Copyright (c) 2015 DarkShine. All rights reserved.
//

import UIKit
import Parse

class TeamTableViewController:UIViewController{
    var league:League!
    var teams = [Team]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        loadTeams()
    }
    
    func loadTeams(){
        if let query = Team.query(){
            query.whereKey("league", equalTo: self.league)
            query.findObjectsInBackgroundWithBlock({ (result, error) -> Void in
                if (error == nil){
                    if let teamsArray = result as? [Team]{
                        self.teams = teamsArray
                        self.tableView.reloadData()
                    }
                }else{
                    print(error)
                }
            })
        }
    }
}

extension TeamTableViewController:UITableViewDelegate{
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if let user = PFUser.currentUser(){
            user["team"] = self.teams[indexPath.row]
            user.saveInBackgroundWithBlock({ (success, error) -> Void in
                if (error == nil){
                    let sb = UIStoryboard(name: "Main", bundle: nil)
                    if let vc = sb.instantiateInitialViewController(){
                        self.presentViewController(vc, animated: true, completion: nil)
                    }
                } else{
                    print(error)
                }
            })
        }
    }
}

extension TeamTableViewController:UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.teams.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let kReuseIdentifier = "teamCell"
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: kReuseIdentifier)
        cell.textLabel?.text = self.teams[indexPath.row].name
        
        return cell
    }
}