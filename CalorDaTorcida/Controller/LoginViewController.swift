//
//  LoginViewController.swift
//  CalorDaTorcida
//
//  Created by Henrique Valcanaia on 9/10/15.
//  Copyright (c) 2015 DarkShine. All rights reserved.
//

import UIKit
import Parse

extension String{
    
    /**
    Return the `Float` value of the current string. Uses the property `doubleValue` of `NSString`, so it presents the same behavior.
    
    - returns: `Float`
    */
    func toFloat() -> Float{
        let nsString = NSString(string: self)
        return nsString.floatValue
    }
    
    /**
    Return the `Double` value of the current string. Uses the property `doubleValue` of `NSString`, so it presents the same behavior.
    
    - returns: `Double`
    */
    func toDouble() -> Double{
        let nsString = NSString(string: self)
        return nsString.doubleValue
    }
}

extension UIView{
    
    @available(iOS, introduced=7.0)
    func addParallaxEffect(){
        let intensity = 35
        let verticalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.y", type: UIInterpolatingMotionEffectType.TiltAlongVerticalAxis)
        verticalMotionEffect.minimumRelativeValue = intensity*(-1)
        verticalMotionEffect.maximumRelativeValue = intensity
        
        let horizontalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.x", type: UIInterpolatingMotionEffectType.TiltAlongHorizontalAxis)
        horizontalMotionEffect.minimumRelativeValue = intensity*(-1)
        horizontalMotionEffect.maximumRelativeValue = intensity
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [verticalMotionEffect, horizontalMotionEffect]
        self.addMotionEffect(group)
    }
    
}

let facebookPermissions = ["email", "public_profile", "user_friends"];

class LoginViewController: UIViewController {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var fbLoginButton: UIButton!
    
    let leaguesVC = LeaguesTableViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        self.backgroundImageView.center = self.view.center
        self.backgroundImageView.addParallaxEffect()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
    }
    
    @IBAction func loginWithFacebook(sender: AnyObject) {
        //        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.Dark)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.Gradient)
        //        SVProgressHUD.showWithStatus("Entrando...")
        PFFacebookUtils.logInInBackgroundWithReadPermissions(facebookPermissions) { (user: PFUser?, error: NSError?) -> Void in
            if let user = user {
                if user.isNew {
                    print("User signed up and logged in through Facebook!")
                } else {
                    print("User logged in through Facebook!")
                }
                SVProgressHUD.showWithStatus("Buscando dados do Facebook...")
                self.loadDataFromFacebook({ (suc:Bool, error:NSError?) -> Void in
                    SVProgressHUD.dismiss()
                    if error == nil {
                        let storyboard = UIStoryboard(name: "Login", bundle: nil)
                        let vc = storyboard.instantiateViewControllerWithIdentifier("LeagueVC")
                        UIView.animateWithDuration(5, animations: { () -> Void in
                            self.backgroundImageView.center.x -= 350
                            self.fbLoginButton.center.x += 350
                            
                            self.view.addSubview(self.leaguesVC.view)
                            
                        })
                        self.presentViewController(vc, animated: true, completion: nil)
                    } else {
                        print(error)
                    }
                })
            } else {
                SVProgressHUD.showErrorWithStatus("Ocorreu um erro ao entrar :( \n \(error?.localizedDescription)")
            }
        }
    }
    
    func loadDataFromFacebook(completionBlock:PFBooleanResultBlock){
        if ((FBSDKAccessToken.currentAccessToken()) != nil){
            let request = FBSDKGraphRequest(graphPath: "me", parameters: nil)
            request.startWithCompletionHandler({ (connection, result, error) -> Void in
                if error == nil {
                    print(result)
                    let data = result as! NSDictionary
                    let user = PFUser.currentUser()!
                    user.username = data["name"] as? String
                    user.saveInBackgroundWithBlock({ (result, error) -> Void in
                        completionBlock(result, error)
                    })
                } else {
                    completionBlock(false, error)
                }
            })
        }else{
            let error = NSError(domain: "io.darkshine", code: 1, userInfo: [NSLocalizedDescriptionKey:"Access token de acesso ao Facebook está nil"])
            completionBlock(false, error)
        }
        
    }
}
