//
//  ScreamViewController.swift
//  CalorDaTorcida
//
//  Created by Henrique Valcanaia on 8/27/15.
//  Copyright (c) 2015 DarkShine. All rights reserved.
//

import UIKit
import Parse
import AVFoundation

class ScreamViewController: UIViewController {
    
    @IBOutlet weak var localTeamCrest: CircularImageView!
    @IBOutlet weak var localTeamName: UILabel!
    @IBOutlet weak var awayTeamName: UILabel!
    @IBOutlet weak var graphView: CircularGraphView!
    @IBOutlet weak var currentLabel: UILabel!
    @IBOutlet weak var peakLabel: UILabel!
    @IBOutlet weak var audiometer: UIProgressView!
    @IBOutlet weak var awayTeamCrest: CircularImageView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    var recorder: AVAudioRecorder!
    var player:AVAudioPlayer!
    var peak: Float = -160.0
    var meterTimer:NSTimer!
    var soundFileURL:NSURL?
    var game:Game!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundImageView.addParallaxEffect()
        loadTeam()
        graphView.backgroundColor = UIColor.clearColor()
    }
    
    func loadTeam(){
        SettingsManager.sharedInstance.testLogin()
        SettingsManager.sharedInstance.loadTeam { (suc: Bool, error: NSError?) -> Void in
            if error == nil{
                self.loadGames()
            }else{
                print(error?.localizedDescription)
            }
        }
    }
    
    func loadGames(){
        if let query = Game.query(){
            query.orderByAscending("date")
            let team = SettingsManager.sharedInstance.team
            query.whereKey("away", equalTo: team)
            query.includeKey("local")
            query.includeKey("away")
            query.getFirstObjectInBackgroundWithBlock({ (object:PFObject?, error:NSError?) -> Void in
                if error == nil{
                    if let unwrappedObj = object{
                        self.game = unwrappedObj as! Game
                        self.updateImages()
                    }
                }else{
                    print(error?.localizedDescription)
                }
            })
        }
    }
    
    func map(x:NSNumber, inMin:NSNumber, inMax:NSNumber, outMin:NSNumber, outMax:NSNumber) -> NSNumber{
        return (x.floatValue - inMin.floatValue) * (outMax.floatValue - outMin.floatValue) / (inMax.floatValue - inMin.floatValue) + outMin.floatValue
    }
    
    func updateAudioMeter(timer:NSTimer) {
        if recorder.recording {
            recorder.updateMeters()
            let apc0 = recorder.averagePowerForChannel(0)
            let peak0 = recorder.peakPowerForChannel(0)
            let mappedValue = map(apc0, inMin: -80, inMax: 100, outMin: 0, outMax: 1)
            
            UIView.animateWithDuration(0.15) {
                self.graphView.setPercentage(mappedValue.doubleValue, animated: true)
                self.audiometer.setProgress(mappedValue.floatValue, animated: true)
            }
            
            peak = max(peak, peak0)
            peakLabel.text = "\(peak)"
            currentLabel.text = "\(apc0)"
        }
    }
    
    @IBAction func medir(sender: AnyObject) {
        if player != nil && player.playing {
            player.stop()
        }
        
        if recorder == nil {
            print("recording. recorder nil")
            recordWithPermission(true)
            return
        }
        
        if recorder != nil && recorder.recording {
            print("pausing")
            recorder.pause()
            
        } else {
            print("recording")
            recordWithPermission(false)
        }
    }
    
    func setupRecorder() {
        let format = NSDateFormatter()
        format.dateFormat="yyyy-MM-dd-HH-mm-ss"
        let currentFileName = "recording-\(format.stringFromDate(NSDate())).m4a"
        print(currentFileName)
        
        var dirPaths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let docsDir: AnyObject = dirPaths[0]
        let soundFilePath = docsDir.stringByAppendingPathComponent(currentFileName)
        soundFileURL = NSURL(fileURLWithPath: soundFilePath)
        let filemanager = NSFileManager.defaultManager()
        if filemanager.fileExistsAtPath(soundFilePath) {
            // probably won't happen. want to do something about it?
            print("sound exists")
        }
        
        do {
            let recordSettings: [String:AnyObject] = [
                AVFormatIDKey: Int(kAudioFormatAppleLossless),
                AVEncoderAudioQualityKey : AVAudioQuality.Max.rawValue,
                AVEncoderBitRateKey : 320000,
                AVNumberOfChannelsKey: 2,
                AVSampleRateKey : 44100.0
            ] 
            
            recorder = try AVAudioRecorder(URL: soundFileURL!, settings: recordSettings )
            recorder.delegate = self
            recorder.meteringEnabled = true
            recorder.prepareToRecord() // creates/overwrites the file at soundFileURL
        } catch let error as NSError {
            print(error.localizedDescription)
            recorder = nil
        }
    }
    
    func recordWithPermission(setup:Bool) {
        let session:AVAudioSession = AVAudioSession.sharedInstance()
        // ios 8 and later
        if (session.respondsToSelector("requestRecordPermission:")) {
            AVAudioSession.sharedInstance().requestRecordPermission({(granted: Bool)-> Void in
                if granted {
                    print("Permission to record granted")
                    self.setSessionPlayAndRecord()
                    if setup {
                        self.setupRecorder()
                    }
                    self.recorder.record()
                    self.meterTimer = NSTimer.scheduledTimerWithTimeInterval(0.1,
                        target:self,
                        selector:"updateAudioMeter:",
                        userInfo:nil,
                        repeats:true)
                } else {
                    print("Permission to record not granted")
                }
            })
        } else {
            print("requestRecordPermission unrecognized")
        }
    }
    
    func setSessionPlayAndRecord() {
        let session:AVAudioSession = AVAudioSession.sharedInstance()
        var error: NSError?
        do {
            try session.setCategory(AVAudioSessionCategoryPlayAndRecord)
        } catch let error1 as NSError {
            error = error1
            print("could not set session category")
            if let e = error {
                print(e.localizedDescription)
            }
        }
        do {
            try session.setActive(true)
        } catch let error1 as NSError {
            error = error1
            print("could not make session active")
            if let e = error {
                print(e.localizedDescription)
            }
        }
    }
    
    func updateImages(){
        let local:Team = game.local
        let away:Team = game.away
        localTeamName.text! = local.name
        awayTeamName.text! = away.name
        
        if let strUrl = local.crest.url{
            if let url = NSURL(string: strUrl){
                self.localTeamCrest.imageURL = url
            }
        }
        
        if let strUrl = away.crest.url{
            if let url = NSURL(string: strUrl){
                self.awayTeamCrest.imageURL = url
            }
        }
    }
    
}

//MARK: - AVAudioRecorderDelegate
extension ScreamViewController: AVAudioRecorderDelegate {
    
    func audioRecorderDidFinishRecording(recorder: AVAudioRecorder,
        successfully flag: Bool) {
            print("finished recording \(flag)")
            //            recordButton.setTitle("GO", forState:.Normal)
            
            // iOS8 and later
            let alert = UIAlertController(title: "Recorder",
                message: "Finished Recording",
                preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Keep", style: .Default, handler: {action in
                print("keep was tapped")
            }))
            alert.addAction(UIAlertAction(title: "Delete", style: .Default, handler: {action in
                print("delete was tapped")
                self.recorder.deleteRecording()
            }))
            self.presentViewController(alert, animated:true, completion:nil)
    }
    
    func audioRecorderEncodeErrorDidOccur(recorder: AVAudioRecorder, error: NSError?) {
        print("\(error!.localizedDescription)")
    }
    
}

// MARK: - AVAudioPlayerDelegate
extension ScreamViewController: AVAudioPlayerDelegate {
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        print("finished playing \(flag)")
    }
    
    func audioPlayerDecodeErrorDidOccur(player: AVAudioPlayer, error: NSError?) {
        print("\(error!.localizedDescription)")
    }
    
}

