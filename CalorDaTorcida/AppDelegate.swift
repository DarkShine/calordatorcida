//
//  AppDelegate.swift
//  CalorDaTorcida
//
//  Created by Pietro Degrazia on 8/27/15.
//  Copyright (c) 2015 DarkShine. All rights reserved.
//

import UIKit
import Parse
import Bolts

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    private let kParseApplicationID = "IhJrR8hS9aXAcsh2aXNRaphf2Eh8uyebjc6piJJp"
    private let kParseClientKey = "Bo8J2ovWeThmHQSiEjBvB1nU3wnJVPnUhWgOs2in"
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        configureParse(application, launchOptions: launchOptions)
        return true
    }
    
}

//MARK: - Parse
extension AppDelegate{
    
    func configureParse(application: UIApplication, launchOptions: [NSObject: AnyObject]?){
        Team.registerSubclass()
        Game.registerSubclass()
        Scream.registerSubclass()
        League.registerSubclass()
        
        Parse.enableLocalDatastore()
        Parse.setApplicationId(kParseApplicationID, clientKey: kParseClientKey)
        PFFacebookUtils.initializeFacebookWithApplicationLaunchOptions(launchOptions)

        PFAnalytics.trackAppOpenedWithLaunchOptionsInBackground(launchOptions, block: { (success: Bool, error: NSError?) -> Void in
            if error != nil{
                print(error?.description)
            }
        })
        
        configureParsePush(application, launchOptions: launchOptions)
    }
    
    func application(application: UIApplication,
        openURL url: NSURL,
        sourceApplication: String?,
        annotation: AnyObject) -> Bool {
            return FBSDKApplicationDelegate.sharedInstance().application(application,
                openURL: url,
                sourceApplication: sourceApplication,
                annotation: annotation)
    }
    
    
    //MARK: watch app methods
    
    func applicationDidBecomeActive(application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }
    
    func configureParsePush(application: UIApplication, launchOptions: [NSObject: AnyObject]?){
        // Register for Push Notitications
        if application.applicationState != UIApplicationState.Background {
            // Track an app open here if we launch with a push, unless
            // "content_available" was used to trigger a background push (introduced in iOS 7).
            // In that case, we skip tracking here to avoid double counting the app-open.
            
            let preBackgroundPush = !application.respondsToSelector("backgroundRefreshStatus")
            let oldPushHandlerOnly = !self.respondsToSelector("application:didReceiveRemoteNotification:fetchCompletionHandler:")
            var pushPayload = false
            if let options = launchOptions {
                pushPayload = options[UIApplicationLaunchOptionsRemoteNotificationKey] != nil
            }
            if (preBackgroundPush || oldPushHandlerOnly || pushPayload) {
                PFAnalytics.trackAppOpenedWithLaunchOptionsInBackground(launchOptions, block: { (suc:Bool, error:NSError?) -> Void in
                    if error != nil{
                        print("Error trackAppOpenedWithLaunchOptionsInBackground: \(error?.localizedDescription)")
                    }
                })
            }
        }
        
        if application.respondsToSelector("registerUserNotificationSettings:") {
            let userNotificationTypes: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]
            let settings = UIUserNotificationSettings(forTypes: userNotificationTypes, categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        } else {
            let types: UIRemoteNotificationType = [UIRemoteNotificationType.Badge, UIRemoteNotificationType.Alert, UIRemoteNotificationType.Sound]
            application.registerForRemoteNotificationTypes(types)
        }
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let installation = PFInstallation.currentInstallation()
        installation.setDeviceTokenFromData(deviceToken)
        installation.saveInBackgroundWithBlock { (suc:Bool, error:NSError?) -> Void in
            if error != nil{
                print("didRegisterForRemoteNotificationsWithDeviceToken: \(error?.localizedDescription)")
            }
        }
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        if error.code == 3010 {
            print("Push notifications are not supported in the iOS Simulator.")
        } else {
            print("application:didFailToRegisterForRemoteNotificationsWithError: %@", error)
        }
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        PFPush.handlePush(userInfo)
        if application.applicationState == UIApplicationState.Inactive {
            PFAnalytics.trackAppOpenedWithRemoteNotificationPayloadInBackground(userInfo, block: { (suc:Bool, error:NSError?) -> Void in
                if error != nil{
                    print("didReceiveRemoteNotification: \(error?.localizedDescription)")
                }
            })
        }
    }
}