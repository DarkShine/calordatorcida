//
//  Game.swift
//  CalorDaTorcida
//
//  Created by Pietro Degrazia on 8/27/15.
//  Copyright (c) 2015 DarkShine. All rights reserved.
//

import Parse

class Game:PFObject, PFSubclassing{
    @NSManaged var away: Team!
    @NSManaged var date: NSDate!
    @NSManaged var local: Team!
    
    override static func query() -> PFQuery?{
        let query:PFQuery = super.query()!
        return query
    }
    
    static func parseClassName() -> String {
        return "Game"
    }
}
