//
//  Scream.swift
//  CalorDaTorcida
//
//  Created by Pietro Degrazia on 8/27/15.
//  Copyright (c) 2015 DarkShine. All rights reserved.
//

import Parse

class Scream:PFObject, PFSubclassing{
    @NSManaged var team: Team!
    @NSManaged var user: PFUser!

    static func parseClassName() -> String {
        return "Scream"
    }
}
