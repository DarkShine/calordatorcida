//
//  League.swift
//  CalorDaTorcida
//
//  Created by Pietro Degrazia on 9/10/15.
//  Copyright (c) 2015 DarkShine. All rights reserved.
//

import Parse

class League:PFObject, PFSubclassing{
    
    @NSManaged var name: String!
    @NSManaged var country: String!
    
    static func parseClassName() -> String {
        return "League"
    }
}