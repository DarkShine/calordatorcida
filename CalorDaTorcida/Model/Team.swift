//
//  Team.swift
//  CalorDaTorcida
//
//  Created by Pietro Degrazia on 8/27/15.
//  Copyright (c) 2015 DarkShine. All rights reserved.
//

import Parse

class Team:PFObject, PFSubclassing{
    @NSManaged var crest: PFFile!
    @NSManaged var name: String!
    @NSManaged var stadium: String!
    @NSManaged var league: League!

    
    override static func query() -> PFQuery?{
        let query:PFQuery = super.query()!
        return query
    }
    
    
    static func parseClassName() -> String {
        return "Team"
    }
}

