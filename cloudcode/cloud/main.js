// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
function isArray(what) {
    return Object.prototype.toString.call(what) === '[object Array]';
}
 
function getRU(ruName) {
    console.log(ruName);
    var RU = Parse.Object.extend("RU");
    var query = new Parse.Query(RU);
    query.equalTo("name", ruName);
    query.limit(1);
    query.find({
        success: function (results) {
            console.log("getRU: " + results[0]);
            return results[0];
        },
        error: function (error) {
            console.log("Error: " + error.code + " " + error.message);
            return false;
        }
    });
}
 
function getMenuItems(items) {
    console.log(items);
    var array = [];
    if (!isArray(items))
        return items;
 
    for (var i = 0; i < items.length; i++) {
        array.push(items[i]);
    }
    return array;
}
 
Parse.Cloud.define('teste', function (request, response) {
    Parse.Cloud.httpRequest({
        url: 'http://www.futebits.com.br/ws/api/getIdentificadorEquipes',
        success: function (httpResponse) {
            var json = JSON.parse(httpResponse.text);
            console.log(json)
            response.success("miau");
            var RU = Parse.Object.extend("RU");
            var Menu = Parse.Object.extend("Menu");
            var ruName = null;
            var items = null;
            var final = json.results.length;
 
            json.results.forEach(function (obj) {
                ruName = obj.value;
                console.log(ruName);
                var query = new Parse.Query(RU);
                query.equalTo("name", ruName);
                query.limit(1);
                query.find({
                    success: function (ruReg) {
                        if (ruReg.length > 0) {
                            console.log(ruReg[0].get("name") + " - " + ruName);
                            items = obj.area_descriptions;
                            var menu = new Menu();
                            menu.set("items", items);
                            menu.set("ru", ruReg[0]);
                            menu.save(null, {
                                success: function (menu) {
                                    console.log("Cardapido do '" + ruName + "' salvo com sucesso");
                                    final--;
                                    if (final <= 0)
                                        response.success("foi");
                                },
                                error: function (menu, error) {
                                    console.log("Erro ao salvar cardapido do '" + ruName + "'! " + error.message);
                                }
                            });
                        } else {
                            console.log("RU '" + ruName + "' nao cadastrado!\n" + ru);
                        }
                    },
                    error: function (error) {
                        console.log("Erro: " + error.message);
                    }
 
                })
            });
 
//            for (var i = 0; i < json.results.length; i++) {
//                ruName = json.results[i].value;
//                items = json.results[i].area_descriptions;
//                var query = new Parse.Query(RU);
//                query.equalTo("name", ruName);
//                query.limit(1);
//                query.find({
//                    success: function (ru) {
//                        if (ru.length > 0) {
//                            var menu = new Menu();
//                            menu.set("items", items);
//                            menu.set("ru", ru[0]);
//                            menu.save(null, {
//                                success: function (menu) {
//                                    console.log("Cardapido do '" + ruName + "' salvo com sucesso");
//                                    final--;
//                                    if(final <= 0)
//                                        response.success("foi");
//                                },
//                                error: function (menu, error) {
//                                    console.log("Erro ao salvar cardapido do '" + ruName + "'! " + error.message);
//                                }
//                            });
//                        } else {
//                            console.log("RU '" + ruName + "' nao cadastrado!\n" + ru);
//                        }
//                    },
//                    error: function (error) {
//                        console.log("Erro: " + error.message);
//                    }
//
//                });
//            }
//            response.success("sucesso");
        },
        error: function (httpResponse) {
            console.error('Request failed with response code ' + httpResponse.status);
        }
    });
});

Parse.Cloud.job("teste", function(request, status) {
	Parse.Cloud.httpRequest({
        url: 'http://www.futebits.com.br/ws/api/getIdentificadorEquipes',
        success: function (httpResponse) {
            var json = JSON.parse(httpResponse.text);
            // console.log(json)
            json.results.forEach(function(obj){
            	console.log(obj);
            });


            // var RU = Parse.Object.extend("RU");
            // var Menu = Parse.Object.extend("Menu");
            // var ruName = null;
            // var items = null;
            // var final = json.results.length;
 
            // json.results.forEach(function (obj) {
            //     ruName = obj.value;
            //     console.log(ruName);
            //     var query = new Parse.Query(RU);
            //     query.equalTo("name", ruName);
            //     query.limit(1);
            //     query.find({
            //         success: function (ruReg) {
            //             if (ruReg.length > 0) {
            //                 console.log(ruReg[0].get("name") + " - " + ruName);
            //                 items = obj.area_descriptions;
            //                 var menu = new Menu();
            //                 menu.set("items", items);
            //                 menu.set("ru", ruReg[0]);
            //                 menu.save(null, {
            //                     success: function (menu) {
            //                         console.log("Cardapido do '" + ruName + "' salvo com sucesso");
            //                         final--;
            //                         if (final <= 0)
            //                             response.success("foi");
            //                     },
            //                     error: function (menu, error) {
            //                         console.log("Erro ao salvar cardapido do '" + ruName + "'! " + error.message);
            //                     }
            //                 });
            //             } else {
            //                 console.log("RU '" + ruName + "' nao cadastrado!\n" + ru);
            //             }
            //         },
            //         error: function (error) {
            //             console.log("Erro: " + error.message);
            //         }
 
            //     })
            // });
        },
        error: function (httpResponse) {
            console.error('Request failed with response code ' + httpResponse.status);
        }
    });
});